/**
 * Created by Ghost on 07.10.2015.
 */
public class User {
    String email;
    String password;
    String gender;
    String subscription;
    String information;


    public User (String email, String password, String gender, String subscription, String information) {
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.subscription = subscription;
        this.information = information;
    }

    public User(){
    }
}
