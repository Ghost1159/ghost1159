import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Ghost on 05.10.2015.
 */
@WebServlet(name = "Servlet2")
public class Servlet2 extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.println(getPageCode());
}

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.println(getPageCode());
        for (String user : UserService.getAll()){
            pw.print("<br>"+user);
        }
    }

    protected String getPageCode() {
        return "<!DOCTYPE html><html>"
                + "<head><meta charset='UTF-8'><title>Registration</title></head>"
                + "<body>"
                + "</body></html>";

    }
}
