/**
 * Created by Ghost on 07.11.2015.
 */
public class Post {
    private String text;
    private String date;
    private String email;

    public Post (String text, String date){
        this.text = text;
        this.date = date;
    }
    public Post (String text, String date, String email){
        this.text = text;
        this.date = date;
        this.email = email;
    }
    public String getText(){
        return text;
    }
    public String getDate(){
        return date;
    }
    public String getEmail() {return email;}
}