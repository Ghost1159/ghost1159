import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Created by Ghost on 24.10.2015.
 */
@WebServlet(name = "Authentification")
public class Authentification extends HttpServlet {

    static int ID;
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String email;
        String password;
        email = request.getParameter("email");
        password = request.getParameter("password");
        try {
            if (UserService.authentification(email, password)){
                ID = UserService.getUserID(email);
                response.sendRedirect("/Profile");
                return;
           }
            else{
                request.setAttribute("match", "<p>Invalid email and/or password.</p> <br> Please, check details or <a href=\"/registration.jsp\">register</a>");
           }
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("Authentification.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("Authentification.jsp").forward(request, response);
    }
}
