import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.sql.SQLException;


/**
 * Created by Ghost on 04.10.2015.
 */
@WebServlet(name = "Registration")
public class Registration extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        User user = new User();
        user.email = request.getParameter("email");
        user.password = request.getParameter("password");
        user.gender = request.getParameter("gender");
        user.subscription = request.getParameter("checkbox") == null ? "Unsubscribed" : "Subscribed";
        user.information = request.getParameter("inf");
            try {
                DbExeption.passwordValidation(user.password);
                DbExeption.emailValidation(user.email);
                UserService.addEntry(user);
                Authentification.ID = UserService.getUserID(user.email);
                response.sendRedirect("/Profile");
                return;
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (SQLException e) {
                request.setAttribute("ex", "<p>This user already exist!</p>");
            } catch (DbExeption e) {
                request.setAttribute("ex", "<p>Invalid password or email!</p>");
            }
        request.getRequestDispatcher("registration.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        request.getRequestDispatcher("registration.jsp").forward(request, response);

    }

}

