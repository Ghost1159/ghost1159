import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;


/**
 * Created by Ghost on 04.10.2015.
 */
@WebServlet(name = "Servlet")
public class Servlet extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        User user = new User();
        user.email = request.getParameter("email");
        user.password = request.getParameter("password");
        user.gender = request.getParameter("gender");
        user.subscription = request.getParameter("checkbox") == null ? "unsubscribed" : "subscribed";
        try {
            DuplicateEntryExeption.Duplicate(request.getParameter("email"));
            DbExeption.emailValidation(request.getParameter("email"));
            try {
                DbExeption.passwordValidation(request.getParameter("password"));
                UserService.addEntry(user);

            }catch (DbExeption dbExeption){
                pw.println("<h3>invalid password</h3>");
            }

        } catch (DuplicateEntryExeption duplicateEntryExeption) {
            pw.println("<h3>already exist</h3>");

        } catch (DbExeption dbExeption) {
            pw.println("<h3>invalid e-mail</h3>");
        }
        pw.println(getPageCode());

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        pw.println(getPageCode());

    }
    protected String getPageCode() {
        return "<!DOCTYPE html><html>"
                + "<head><meta charset='UTF-8'><title>Registration</title>"
                + "<style>"
                + ".main { text-align: center}"
                + "h3 { text-align:center ; color: #333}"
                + "</style>"
                + "</head>"
                + "<body>"
                + "<div class='main'>"
                + "<form action='registration' method='POST'>"
                + "<input type='text' name='email' placeholder='e-mail'>"
                + "<br><br>"
                + "<input type='text' name='password' placeholder='password'>"
                + "<br> <br> Gender:<br>"
                + "<input type='radio' name='gender' value='male' checked='checked'> Male"
                + "<br>"
                + "<input type='radio' name='gender' value='female'> Female"
                + "<br> <br>"
                + "<input type='checkbox' name='checkbox' value='subscribed' checked='checked'> Subscribe"
                + "<br> <br>"
                + "<input type='submit' value='send'>"
                + "</form> <br><br>"
                + "<a href='/userslist'> Show data </a>  "
                + "</div>"
                + "</body></html>";
    }
}

