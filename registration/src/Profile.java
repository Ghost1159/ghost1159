import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.*;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Created by Ghost on 05.10.2015.
 */
@WebServlet(name = "Profile")
public class Profile extends HttpServlet {
    @Override
    public void init() throws ServletException {
        super.init();
    }
    ArrayList<Post> posts = new ArrayList<Post>();
    ArrayList<Post> allPosts = new ArrayList<Post>();
    private User user = new User();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            UserService.addPost(request.getParameter("post"),Authentification.ID);
            response.sendRedirect("/Profile");
            return;
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        request.setAttribute("email", user.email);
        request.setAttribute("gender", user.gender);
        request.setAttribute("subscription", user.subscription);
        request.setAttribute("info", user.information);
        request.setAttribute("posts",posts);
        request.setAttribute("all_posts",allPosts);
        request.getRequestDispatcher("Profile.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            user  = UserService.getInf(Authentification.ID);
            posts = UserService.getPosts(Authentification.ID);
            allPosts = UserService.getAllPosts();
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        request.setAttribute("email", user.email);
        request.setAttribute("gender", user.gender);
        request.setAttribute("subscription", user.subscription);
        request.setAttribute("info", user.information);
        request.setAttribute("posts",posts);
        request.setAttribute("all_posts",allPosts);
        request.getRequestDispatcher("Profile.jsp").forward(request, response);
    }

}
