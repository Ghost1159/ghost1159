
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * Created by Ghost on 07.10.2015.
 */
public class UserService {
    public static void addEntry(User user) throws IOException, SQLException, ClassNotFoundException {
        Connection connection = DataBase.getConnection();
        PreparedStatement ps = connection.prepareStatement("insert into users(email, password, sex, subscription, information) values (?,?,?,?,?)");
        ps.setString(1, user.email);
        ps.setString(2, user.password);
        ps.setString(3, user.gender);
        ps.setString(4, user.subscription);
        ps.setString(5, user.information);
        ps.execute();
    }
    public static boolean authentification (String email, String password)throws SQLException, ClassNotFoundException {
        Connection conn = DataBase.getConnection();
        PreparedStatement ps = conn.prepareStatement("select email from users where password = ? and email = ?");
        ps.setString(1, password);
        ps.setString(2, email);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            if (rs.wasNull()) {
                return false;
            } else
                return true;
        }
        return false;
    }
    public static int getUserID (String email)throws SQLException, ClassNotFoundException{
        Connection conn = DataBase.getConnection();
        int current = 0;
        PreparedStatement ps = conn.prepareStatement("select email, id from users where email = ?");
        ps.setString(1, email);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            current = rs.getInt("id");
        }
        return current;
    }
    public static User getInf (int id)throws SQLException, ClassNotFoundException {
        User current = new User();
        Connection conn = DataBase.getConnection();
        PreparedStatement ps = conn.prepareStatement("select email, sex, subscription, information from users where id = ?");
        ps.setString(1, String.valueOf(id));
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            current.email = rs.getString("email");
            current.gender = rs.getString("sex");
            current.subscription = rs.getString("subscription");
            current.information = rs.getString("information");
        }
        return current;
    }
    public static void addPost(String post, int id) throws IOException, SQLException, ClassNotFoundException {
        Connection connection = DataBase.getConnection();
        PreparedStatement ps = connection.prepareStatement("insert into posts(id, post) values (?,?)");
        ps.setString(1, String.valueOf(id));
        ps.setString(2, post);
        ps.execute();
    }
    public static ArrayList<Post> getPosts (int id) throws IOException, SQLException, ClassNotFoundException{
        ArrayList <Post> posts = new  ArrayList<Post>();
        Connection conn = DataBase.getConnection();
        PreparedStatement ps = conn.prepareStatement("select post, date from posts where id = ?");
        ps.setString(1, String.valueOf(id));
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Post post = new Post(rs.getString("post"), rs.getString("date"));
            posts.add(post);
        }
        return posts;
    }
    public static ArrayList<Post> getAllPosts () throws IOException, SQLException, ClassNotFoundException{
        ArrayList <Post> posts = new  ArrayList<Post>();
        Connection conn = DataBase.getConnection();
        PreparedStatement ps = conn.prepareStatement("select * from posts ");
        ResultSet rs = ps.executeQuery();
        while(rs.next()){
            Post post = new Post(rs.getString("post"), rs.getString("date"));
            posts.add(post);
        }
        return posts;
    }
}
