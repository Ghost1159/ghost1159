<%--
  Created by IntelliJ IDEA.
  User: Ghost
  Date: 01.11.2015
  Time: 23:01
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html>
<head>
    <title>Profile</title>
    <script src="jquery-2.1.4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="ProfileStyle.css">
</head>
<body>
 <div id="inf">
     Email: ${email}.<br><br>
     ${gender} <br><br>
     ${subscription}<br><br>
     Information: ${info} <br><br>
     <form action='Profile' method='POST'>
       <textarea name="post" placeholder="your's massage"></textarea><br><br>
       <input id = 'button' type='submit' value='Send post'>
     </form>
 </div>
 <div id="my_posts">
     <br>
     <h2>My posts:</h2>
        <ul type="circle">
             <c:choose>
                 <c:when test="${fn:length(posts) gt 0}">
                    <c:forEach items="${posts}" var="post">
                        <li>
                            ${post.getText()}
                                <br>
                            ${post.getDate()}
                        </li><br>
                    </c:forEach>
                 </c:when>
                 <c:otherwise>
                     <h3>You have no posts</h3>
                 </c:otherwise>
             </c:choose>
         </ul>
 </div>
 <div id="all_posts">
     <br>
     <h2>All posts:</h2>
     <ul>
         <c:choose>
             <c:when test="${fn:length(all_posts) gt 0}">
                 <c:forEach items="${all_posts}" var="post">
                     <li>
                        ${post.getText()}
                            <br>
                        ${post.getDate()}
                     </li><br>
                 </c:forEach>
             </c:when>
             <c:otherwise>
                 <h3>No posts</h3>
             </c:otherwise>
     </c:choose>
     </ul>
 </div>
</body>
</html>
